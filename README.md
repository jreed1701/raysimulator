Author: Joshua Reed
Date: 8-5-09

To run the RAYSIM GUI, enter main into the MATLAB command window.

For a full description of this GUI read:

RAYSIM_Program_V2.0.doc

Email questions to: jreed1701@gmail.com