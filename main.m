function varargout = main(varargin)
% MAIN M-file for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 03-Aug-2009 14:19:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%% INITIALIZE HERE

%%% Add mfiles directory to path
addPath = sprintf('%s%s',pwd,'/mfiles');
path(addPath,path);


%%

% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;


function erayParams_Callback(hObject, eventdata, handles)
edparam

function gtDelays_Callback(hObject, eventdata, handles)
[filename, path] = uigetfile('Default.mat','Get input');

if ischar(path)
    
    temp = sprintf('%s%s',path,filename); 
    ind = find('.'==filename); % Check file extenstion
    foo = filename(ind:length(filename));
    
    if strcmp(foo,'.mat')
    rzgrid_to_deltat(temp)
    else
        errordlg('This simulation only takes .MAT files. Try again.',...
            'File Extension Error');
    end

end

function rangeDepth_Callback(hObject, eventdata, handles)
RDsim

function srcTrack_Callback(hObject, eventdata, handles)
SrcTrack

function singlePair_Callback(hObject, eventdata, handles)
SinglePair

function recievedSig_Callback(hObject, eventdata, handles)
spRcvdSig


%% Drop-Down Menus
function project_Callback(hObject, eventdata, handles)

function new_Callback(hObject, eventdata, handles)
lbox2
function exit_Callback(hObject, eventdata, handles)
close(findobj('type','figure','name','main'));
close(findobj('type','figure','name','edparam'));
close(findobj('type','figure','name','SinglePair'));
close(findobj('type','figure','name','SrcTrack'));
close(findobj('type','figure','name','RDsim'));
close(findobj('type','figure','name','Lloyd Mirror'));
close(findobj('type','figure','name','Directory List'));


function reset_Callback(hObject, eventdata, handles)


function resetAll_Callback(hObject, eventdata, handles)
load priorRuns
% Reset single pair
sPair.ssp = 1;
sPair.bth = 1;
sPair.range=0;
sPair.zsrc=0;
sPair.zrcvr=0;
sPair.em=0;
% Reset Llyod mirror
LMirror.ssp =1;
LMirror.bth = 1;
LMirror.em = 0;
LMirror.range=0;
LMirror.zsrc=0;
LMirror.zrcvr=0;
LMirror.rms=0;
LMirror.fs =0;
LMirror.fc = 0;
LMirror.np = 0;
% Reset Src Track
srcTrack.ssp = 1;
srcTrack.bth= 1;
srcTrack.track =1;
% Reset RD grid
rdepth.ssp =1;
rdepth.bth = 1;
rdepth.em = 0;
rdepth.rsrcMax = 0;
rdepth.rsrcMin = 0;
rdepth.rsrcStep=0;
rdepth.zsrcMax = 0;
rdepth.zsrcMin =0;
rdepth.zsrcStep=0;
rdepth.zrcvr =0;
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end
function resetSP_Callback(hObject, eventdata, handles)
load priorRuns
% Reset single pair
sPair.ssp = 1;
sPair.bth = 1;
sPair.range=0;
sPair.zsrc=0;
sPair.zrcvr=0;
sPair.em=0;
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end
function resetLM_Callback(hObject, eventdata, handles)
load priorRuns
% Reset Llyod mirror
LMirror.ssp =1;
LMirror.bth = 1;
LMirror.em = 0;
LMirror.range=0;
LMirror.zsrc=0;
LMirror.zrcvr=0;
LMirror.rms=0;
LMirror.fs =0;
LMirror.fc = 0;
LMirror.np = 0;
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end
function resetST_Callback(hObject, eventdata, handles)
load priorRuns
% Reset Src Track
srcTrack.ssp = 1;
srcTrack.bth= 1;
srcTrack.track =1;
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end
function resetRD_Callback(hObject, eventdata, handles)
load priorRuns
% Reset RD grid
rdepth.ssp =1;
rdepth.bth = 1;
rdepth.em = 0;
rdepth.rsrcMax = 0;
rdepth.rsrcMin = 0;
rdepth.rsrcStep=0;
rdepth.zsrcMax = 0;
rdepth.zsrcMin =0;
rdepth.zsrcStep=0;
rdepth.zrcvr =0;
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end