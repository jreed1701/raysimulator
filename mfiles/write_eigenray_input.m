function write_eigenray_input(erayparams,inputfile,version)
%%% This function writes an input file for Brian Dushaw's eigenray
%%% program

%%% Set default version to 2 of eigenray binary
if nargin<3
  version=2;
end

%%% Open file
finput=fopen(inputfile,'w');

fprintf(finput,'%0.1f  %0.1f \n',erayparams.ang1,erayparams.ang2);

if version==1
  fprintf(finput,'%i  %0.1f \n',erayparams.numrays,erayparams.zomit_raytrace);
  fprintf(finput,'%i \n',erayparams.numrays_2ndtrace);
else
  fprintf(finput,'%i \n',erayparams.numrays);
end  

fprintf(finput,'%0.1f  %0.1f \n',erayparams.zs,erayparams.zr);

if version==1
  fprintf(finput,'0.0  %0.1f  \n',erayparams.range);
else
  fprintf(finput,'%0.1f  \n',erayparams.range);
end

fprintf(finput,'%0.1f  \n',erayparams.dxsav);
fprintf(finput,'%0.1f  %0.1f \n',erayparams.zmiss,erayparams.zmissmax);
fprintf(finput,'%i \n',erayparams.iraysave);
fprintf(finput,'%i \n',erayparams.itimefront);
fprintf(finput,'%i \n',erayparams.ibot);
fprintf(finput,'%0.1f  \n',erayparams.ttoffset);
fprintf(finput,'%i \n',erayparams.itemplate);
fprintf(finput,'%s \n',erayparams.sspfile);
fprintf(finput,'%0.1f  \n',erayparams.transdepth);
fprintf(finput,'%0.1f  %0.1f  %0.1f \n',erayparams.stepsize_surf,...
    erayparams.stepsize_trans,erayparams.stepsize_atdepth);
fprintf(finput,'%e  \n',erayparams.epsilon);
fprintf(finput,'%s \n',erayparams.templatefile);
fprintf(finput,'%i \n',erayparams.maxbounce);
fprintf(finput,'%0.1f  \n',erayparams.btol);
fprintf(finput,'%s \n',erayparams.bathymfile);

%%% Close files
fclose(finput);
return
