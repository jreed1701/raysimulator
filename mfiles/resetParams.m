%% Author: Joshua Reed, Date: 6/17/09
% 
%   The purpose of this program is to Load default values from a preset 
%   file to the edparm GUI.
% 
    load paramSettings.mat

    [filename path]=uigetfile('Default.mat','Load User Parameters');
if ischar(path)
    tmp = sprintf('%s%s',path,filename);
    
    %Check extension , Need .mat file
  
    ind = find('.'==filename);
    foo = filename(ind:length(filename));
   
    if ~isempty(path)

            if strcmp(foo,'.mat')    
            
            load(tmp);
            
            set(handles.ang1, 'String', erayparams.ang1);
            set(handles.ang2, 'String', erayparams.ang2);
            set(handles.ray1, 'String', erayparams.numrays);
            set(handles.dxsav, 'String', erayparams.dxsav);
            set(handles.zmiss, 'String', erayparams.zmiss);
            set(handles.zmissmax, 'String', erayparams.zmissmax);
            set(handles.iraysave, 'String', erayparams.iraysave);
            set(handles.itimefront, 'String', erayparams.itimefront);
            set(handles.ibot, 'String', erayparams.ibot);
            set(handles.ttoffset, 'String', erayparams.ttoffset);
            set(handles.itemplate, 'String', erayparams.itemplate);
            set(handles.transdepth, 'String', erayparams.transdepth);
            set(handles.btol, 'String', erayparams.btol);
            set(handles.maxbounce, 'String', erayparams.maxbounce);
            set(handles.ss_surf, 'String', erayparams.stepsize_surf);
            set(handles.ss_trans, 'String', erayparams.stepsize_trans);
            set(handles.ss_atdepth, 'String', erayparams.stepsize_atdepth);
            set(handles.epsilon, 'String', erayparams.epsilon);
            set(handles.template, 'String', erayparams.templatefile);
            set(handles.userFile,'String',filename);
            
            else
               errordlg('You need to select a .MAT file!');
               return
            end
    
    else 
        return
    end
end
        