%% Author: Joshua Reed, Date: 8/20/09
% 
%   function [c] = getSurfSpeed(SSPfile)
% 
%   
% 
% 
function [c] = getSurfSpeed(SSPfile)

    index = find('.'==SSPfile);
    xx = SSPfile(1:index-1);
    
    index = find('/'==xx);
    name = xx(index+1:length(xx));
    
    load(SSPfile)    
    tmp = eval(name);
    tmp = tmp(:,2);
    
    c = tmp(2);

end