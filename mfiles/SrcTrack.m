function varargout = SrcTrack(varargin)
% SRCTRACK M-file for SrcTrack.fig
%      SRCTRACK, by itself, creates a new SRCTRACK or raises the existing
%      singleton*.
%
%      H = SRCTRACK returns the handle to a new SRCTRACK or the handle to
%      the existing singleton*.
%
%      SRCTRACK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SRCTRACK.M with the given input arguments.
%
%      SRCTRACK('Property','Value',...) creates a new SRCTRACK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SrcTrack_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SrcTrack_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SrcTrack

% Last Modified by GUIDE v2.5 03-Aug-2009 16:21:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SrcTrack_OpeningFcn, ...
                   'gui_OutputFcn',  @SrcTrack_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SrcTrack is made visible.
function SrcTrack_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SrcTrack (see VARARGIN)

% Choose default command line output for SrcTrack
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SrcTrack wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%% INITIALIZE HERE
load priorRuns
usr.ssp_index = srcTrack.ssp;
usr.bth_index = srcTrack.bth;
usr.scn_index = srcTrack.track;
usr.measType = 1;
usr.path = pwd;
usr.break = 0;
save('progSettings','usr','handles');

% Initialize SSP popup menu
getSSPFileInfo
set(handles.ssp_popup,'String',ssp);

%Initialize Bathy popup menu
getBTHFileInfo
set(handles.bth_popup, 'String', bth);

%Initialize Track popup menu
getSceneFileInfo
set(handles.track_popup, 'String', scene);

%Initialize prior run;
set(handles.ssp_popup,'Value',srcTrack.ssp);
set(handles.bth_popup,'Value',srcTrack.bth);
set(handles.track_popup,'Value',srcTrack.track);

% Format plot area
xlabel(handles.stAxes,'Range(m)');
ylabel(handles.stAxes,'Depth(m)');
title(handles.stAxes,'Range = ? m');

%%

% --- Outputs from this function are returned to the command line.
function varargout = SrcTrack_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function runSim_Callback(hObject, eventdata, handles)
% Format plot area
cla(handles.stAxes,'reset');
xlabel(handles.stAxes,'Range(m)');
ylabel(handles.stAxes,'Depth(m)');
title(handles.stAxes,'Range = ? m');
set(handles.stAxes,'XTickLabel',{'0','200','400','600','800','1000'});
set(handles.stAxes,'ydir','reverse');
set(handles.stAxes,'YTickLabel',{'0','20','40','60','80','100'});

% Save run
load priorRuns
srcTrack.ssp=get(handles.ssp_popup,'Value');
srcTrack.bth=get(handles.bth_popup,'Value');
srcTrack.track=get(handles.track_popup,'Value');
if isunix
    save('mfiles/priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
else
    save('mfiles\priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
end

raysim

function ssp_popup_Callback(hObject, eventdata, handles)
function ssp_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ssp_enter_Callback(hObject, eventdata, handles)
load progSettings.mat;
index = get(handles.ssp_popup,'Value');
usr.ssp_index = index;
save('progSettings','usr','handles');

function bth_popup_Callback(hObject, eventdata, handles)
function bth_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function bth_enter_Callback(hObject, eventdata, handles)
load progSettings.mat;
index = get(handles.bth_popup,'Value');
usr.bth_index = index;
save('progSettings','usr','handles');

function track_popup_Callback(hObject, eventdata, handles)
function track_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function track_enter_Callback(hObject, eventdata, handles)
load progSettings.mat;
index = get(handles.track_popup,'Value');
usr.scn_index = index;
save('progSettings','usr','handles');



function cancel_Callback(hObject, eventdata, handles)
load progSettings.mat;
usr.break = 1;
save('progSettings','usr','handles');

%% DROP DOWN MENU

function file_Callback(hObject, eventdata, handles)

function reset_Callback(hObject, eventdata, handles)
load priorRuns
% Reset Src Track
srcTrack.ssp = 1;
srcTrack.bth= 1;
srcTrack.track =1;
% Reset fields
set(handles.ssp_popup,'Value',1);
set(handles.bth_popup,'Value',1);
set(handles.track_popup,'Value',1);
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end

function exit_Callback(hObject, eventdata, handles)
close(findobj('type','figure','name','SrcTrack'));

