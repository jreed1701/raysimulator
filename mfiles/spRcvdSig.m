function varargout = spRcvdSig(varargin)
% SPRCVDSIG M-file for spRcvdSig.fig
%      SPRCVDSIG, by itself, creates a new SPRCVDSIG or raises the existing
%      singleton*.
%
%      H = SPRCVDSIG returns the handle to a new SPRCVDSIG or the handle to
%      the existing singleton*.
%
%      SPRCVDSIG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPRCVDSIG.M with the given input arguments.
%
%      SPRCVDSIG('Property','Value',...) creates a new SPRCVDSIG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before spRcvdSig_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to spRcvdSig_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help spRcvdSig

% Last Modified by GUIDE v2.5 18-Aug-2009 15:31:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @spRcvdSig_OpeningFcn, ...
                   'gui_OutputFcn',  @spRcvdSig_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before spRcvdSig is made visible.
function spRcvdSig_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to spRcvdSig (see VARARGIN)

% Choose default command line output for spRcvdSig
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes spRcvdSig wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%% INITIALIZE HERE
load priorRuns


% Initialize program settings
usr.ssp_index = LMirror.ssp;
usr.bth_index = LMirror.bth;
usr.measType = 1;
usr.path = pwd;
save('progSettings','usr','handles');

% Initialize SSP popup menu
getSSPFileInfo
set(handles.ssp_popup,'String',ssp);

%Initialize Bathy popup menu
getBTHFileInfo
set(handles.bth_popup, 'String', bth);

% Load fields from prior run;

set(handles.ssp_popup,'Value',LMirror.ssp);
set(handles.bth_popup,'Value',LMirror.bth);
set(handles.range,'String',num2str(LMirror.range));
set(handles.src_Depth,'String',num2str(LMirror.zsrc));
set(handles.rcvr_Depth,'String',num2str(LMirror.zrcvr));
set(handles.rms,'String',num2str(LMirror.rms));
set(handles.fs,'String',num2str(LMirror.fs));
set(handles.fc,'String',num2str(LMirror.fc));
set(handles.numPoints,'String',num2str(LMirror.np));
set(handles.units,'Value',LMirror.em);
if (LMirror.em == 0)
    set(handles.units,'String','English');
    set(handles.units_r,'String','ft');
    set(handles.units_zsrc,'String','ft');
    set(handles.units_zrcvr,'String','ft');
    set(handles.units_rms,'String','ft');
    load progSettings
    usr.measType = 1;
    save('progSettings','usr','handles');
else
    set(handles.units,'String','Metric');
    set(handles.units_r,'String','m');
    set(handles.units_zsrc,'String','m');
    set(handles.units_zrcvr,'String','m');
    set(handles.units_rms,'String','m');
    load progSettings
    usr.measType = 2;
    save('progSettings','usr','handles');
end

%Initialize Plots

%%% Titles
title(handles.ssp,'SSP File');
title(handles.pair,'The Time Delay is XX (Samples)');
title(handles.rcvdSig,'Simulated Received Signal');
title(handles.auto,'Output of Autocorrelation Processor');

%%% Labels
ylabel(handles.ssp,'Depth (m)');
xlabel(handles.ssp,'SSP (m/s)');
xlabel(handles.pair,'Range (m)');
ylabel(handles.rcvdSig,'y[n]');
ylabel(handles.auto,'R_{yy}[n]');
xlabel(handles.auto,'n');

%%% Tick-Labels
set(handles.ssp,'YTickLabel',{'0','20','40','60','80','100'});
set(handles.ssp,'XTickLabel',{'0','500','1000'});
set(handles.ssp,'ydir','reverse');
set(handles.pair,'YTickLabel',{'0','20','40','60','80','100'});
set(handles.pair,'XTickLabel',{'0','200','400','600','800','1000'});
set(handles.pair,'ydir','reverse');
set(handles.rcvdSig,'YTickLabel',{'-1','0','1'});
set(handles.rcvdSig,'XTickLabel',{'0','200','400','600','800','1000','1200','1400','1600','1800','2000'});
set(handles.auto,'YTickLabel',{'0','100','200'});
set(handles.auto,'XTickLabel',{'-1000','-800','-600','-400','-200','0','200','400','600','800','1000'});


%%


% --- Outputs from this function are returned to the command line.
function varargout = spRcvdSig_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function ssp_enter_Callback(hObject, eventdata, handles)
load progSettings.mat;
index = get(handles.ssp_popup,'Value');
usr.ssp_index = index;
save('progSettings','usr','handles');

function bth_enter_Callback(hObject, eventdata, handles)
load progSettings.mat;
index = get(handles.bth_popup,'Value');
usr.bth_index = index;
save('progSettings','usr','handles');

function ssp_popup_Callback(hObject, eventdata, handles)
function ssp_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function bth_popup_Callback(hObject, eventdata, handles)
function bth_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function units_Callback(hObject, eventdata, handles)
button_state = get(hObject, 'Value');

if button_state == get(hObject, 'Max')
        load progSettings.mat;
        usr.measType = 2; % Indicate Metric
        save('progSettings','usr','handles');
        tmp = str2num(get(handles.range, 'String')); 
        set(handles.range, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.src_Depth, 'String')); 
        set(handles.src_Depth, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.rcvr_Depth, 'String')); 
        set(handles.rcvr_Depth, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.rms, 'String')); 
        set(handles.rms, 'String', feet_to_meters(tmp));
        set(handles.units,'String','Metric');
        set(handles.units_r,'String','m');
        set(handles.units_zsrc,'String','m');
        set(handles.units_zrcvr,'String','m');
        set(handles.units_rms,'String','m');
elseif button_state == get(hObject,'Min')
        load progSettings.mat;
        usr.measType = 1; % Indicate English
        save('progSettings','usr','handles');
        tmp = str2num(get(handles.range, 'String')); 
        set(handles.range, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.src_Depth, 'String')); 
        set(handles.src_Depth, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.rcvr_Depth, 'String')); 
        set(handles.rcvr_Depth, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.rms, 'String')); 
        set(handles.rms, 'String', meters_to_feet(tmp));
        set(handles.units,'String','English');
        set(handles.units_r,'String','ft');
        set(handles.units_zsrc,'String','ft');
        set(handles.units_zrcvr,'String','ft');
        set(handles.units_rms,'String','ft');
end

function range_Callback(hObject, eventdata, handles)
function range_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function src_Depth_Callback(hObject, eventdata, handles)
function src_Depth_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function rcvr_Depth_Callback(hObject, eventdata, handles)
function rcvr_Depth_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function runSim_Callback(hObject, eventdata, handles)
%%% file Plots
cla(handles.ssp,'reset');
cla(handles.pair,'reset');
cla(handles.rcvdSig,'reset');
cla(handles.auto,'reset');
%%% Titles
title(handles.ssp,'SSP File');
title(handles.pair,'The Time Delay is XX (Samples)');
title(handles.rcvdSig,'Simulated Received Signal');
title(handles.auto,'Output of Autocorrelation Processor');
%%% Labels
ylabel(handles.ssp,'Depth (m)');
xlabel(handles.ssp,'SSP (m/s)');
xlabel(handles.pair,'Range (m)');
ylabel(handles.rcvdSig,'y[n]');
ylabel(handles.auto,'R_{yy}[n]');
xlabel(handles.auto,'n');
%%% Tick-Labels
set(handles.ssp,'YTickLabel',{'0','20','40','60','80','100'});
set(handles.ssp,'XTickLabel',{'0','500','1000'});
set(handles.ssp,'ydir','reverse');
set(handles.pair,'YTickLabel',{'0','20','40','60','80','100'});
set(handles.pair,'XTickLabel',{'0','200','400','600','800','1000'});
set(handles.pair,'ydir','reverse');
set(handles.rcvdSig,'YTickLabel',{'-1','0','1'});
set(handles.rcvdSig,'XTickLabel',{'0','200','400','600','800','1000','1200','1400','1600','1800','2000'});
set(handles.auto,'YTickLabel',{'0','100','200'});
set(handles.auto,'XTickLabel',{'-1000','-800','-600','-400','-200','0','200','400','600','800','1000'});

%%% Check Data fields
tmp(1)= str2num(get(handles.range,'String')); 
tmp(2)= str2num(get(handles.src_Depth,'String')); 
tmp(3)= str2num(get(handles.rcvr_Depth,'String'));
tmp(4)= str2num(get(handles.fs,'String')); 
tmp(5)= str2num(get(handles.fc,'String')); 
tmp(6)= str2num(get(handles.numPoints,'String'));

for k = 1:length(tmp)
    if tmp(k) == 0
        errordlg('One or more or your values is Zero.  Use non-zero values.');
        return;
    end
end

if (tmp(5) > tmp(4))
    errordlg('Cutoff frequecy must not be greater than the sampling frequency');
    return
end

% Save run parameters for later
load priorRuns
LMirror.ssp = get(handles.ssp_popup,'Value');
LMirror.bth = get(handles.bth_popup,'Value');
LMirror.em = get(handles.units,'Value');
LMirror.range = str2num(get(handles.range,'String'));
LMirror.zsrc = str2num(get(handles.src_Depth,'String'));
LMirror.zrcvr = str2num(get(handles.rcvr_Depth,'String'));
LMirror.rms = str2num(get(handles.rms,'String'));
LMirror.fs = str2num(get(handles.fs,'String'));
LMirror.fc = str2num(get(handles.fc,'String'));
LMirror.np = str2num(get(handles.numPoints,'String'));
if isunix
    save('mfiles/priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
else
    save('mfiles\priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
end

% Run simulation
rcvdSigSim

function erayparams_Callback(hObject, eventdata, handles)
edparam

function fs_Callback(hObject, eventdata, handles)
function fs_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function fc_Callback(hObject, eventdata, handles)
function fc_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function numPoints_Callback(hObject, eventdata, handles)
function numPoints_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function rms_Callback(hObject, eventdata, handles)
function rms_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function corr_Callback(hObject, eventdata, handles)


function corr_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
%% DROP DOWN MENU
function file_Callback(hObject, eventdata, handles)

function reset_Callback(hObject, eventdata, handles)
load priorRuns
% file Llyod mirror
LMirror.ssp =1;
LMirror.bth = 1;
LMirror.em = 0;
LMirror.range=0;
LMirror.zsrc=0;
LMirror.zrcvr=0;
LMirror.rms=0;
LMirror.fs =0;
LMirror.fc = 0;
LMirror.np = 0;
% Reset Fields
set(handles.ssp_popup,'Value',1);
set(handles.bth_popup,'Value',1);
set(handles.units,'Value',0);
set(handles.units,'String','English');
set(handles.range,'String','0');
set(handles.src_Depth,'String','0');
set(handles.rcvr_Depth,'String','0');
set(handles.rms,'String','0');
set(handles.fs,'String','0');
set(handles.fc,'String','0');
set(handles.numPoints,'String','0');
set(handles.units_r,'String','ft');
set(handles.units_zsrc,'String','ft');
set(handles.units_zrcvr,'String','ft');
set(handles.units_rms,'String','ft');
load progSettings
usr.measType = 1;
save('progSettings','usr','handles');
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end

function Exit_Callback(hObject, eventdata, handles)
close(findobj('type','figure','name','Lloyd Mirror'));
