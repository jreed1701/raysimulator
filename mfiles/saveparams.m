%%% Load into global parameters
            
load paramSettings.mat

FILENAME = 'params.m';
fid = fopen(FILENAME, 'w');
            
            
temp{1} = str2num(get(handles.ang1, 'String'));
temp{2} = str2num(get(handles.ang2, 'String'));
temp{3} = str2num(get(handles.ray1, 'String'));
temp{4} = str2num(get(handles.dxsav, 'String'));
temp{5} = str2num(get(handles.zmiss, 'String'));
temp{6} = str2num(get(handles.zmissmax, 'String'));
temp{7} = str2num(get(handles.iraysave, 'String'));
temp{8} = str2num(get(handles.itimefront, 'String'));
temp{9} = str2num(get(handles.ibot, 'String'));
temp{10} = str2num(get(handles.ttoffset, 'String'));
temp{11} = str2num(get(handles.itemplate, 'String'));
temp{12} = str2num(get(handles.transdepth, 'String'));
temp{13} = str2num(get(handles.btol, 'String'));
temp{14} = str2num(get(handles.maxbounce, 'String'));
temp{15} = str2num(get(handles.ss_surf, 'String'));
temp{16} = str2num(get(handles.ss_trans, 'String'));
temp{17} = str2num(get(handles.ss_atdepth,'String'));
temp{18} = str2num(get(handles.epsilon, 'String'));
temp{19} = get(handles.template,'String');

% Write to programfile
fprintf(fid,'erayparams.ang1=%d;\n',temp{1});
fprintf(fid,'erayparams.ang2=%d;\n',temp{2});
fprintf(fid,'erayparams.numrays=%d;\n',temp{3});
fprintf(fid,'erayparams.dxsav=%d;\n',temp{4});
fprintf(fid,'erayparams.zmiss=%d;\n',temp{5});
fprintf(fid,'erayparams.zmissmax=%d;\n',temp{6});
fprintf(fid,'erayparams.iraysave=%d;\n',temp{7});
fprintf(fid,'erayparams.itimefront=%d;\n',temp{8});
fprintf(fid,'erayparams.ibot=%d;\n',temp{9});
fprintf(fid,'erayparams.ttoffset=%d;\n',temp{10});
fprintf(fid,'erayparams.itemplate=%d;\n',temp{11});
fprintf(fid,'erayparams.transdepth=%d;\n',temp{12});
fprintf(fid,'erayparams.btol=%d;\n',temp{13});
fprintf(fid,'erayparams.maxbounce=%d;\n',temp{14});
fprintf(fid,'erayparams.stepsize_surf=%d;\n',temp{15});
fprintf(fid,'erayparams.stepsize_trans=%d;\n',temp{16});
fprintf(fid,'erayparams.stepsize_atdepth=%d;\n',temp{17});
fprintf(fid,'erayparams.epsilon=%d;\n',temp{18});
fprintf(fid,'erayparams.templatefile=''%s'';\n',temp{19});
fprintf(fid,'erayparams.zs='''';\n');
fprintf(fid,'erayparams.zr='''';\n');
fprintf(fid,'erayparams.range='''';\n');
           