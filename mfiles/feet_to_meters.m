function meters=feet_to_meters(ft)

meters=ft*12*2.54/100;

return;
