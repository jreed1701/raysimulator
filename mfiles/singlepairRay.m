%%% Script file to run ray simulation

clear;
load progSettings.mat;

%%% Specify parameters
rsrcft= str2num(get(handles.sprange,'String')); % Range 
zrcvrft=str2num(get(handles.sprcvrdepth,'String'));% Reciever Depth
zsrcft=str2num(get(handles.spsrcdepth,'String'));  % Source Depth


if rsrcft < 0
    errordlg('Maximum range cannot be negative!','Input Error');
    return
end


if usr.measType == 1 % Have to convert feet to meters
zrcvrm=feet_to_meters(zrcvrft);
zsrcm=feet_to_meters(zsrcft);
rsrcm=feet_to_meters(rsrcft);
else
    zrcvrm=zrcvrft;
    zsrcm=zsrcft;
    rsrcm=rsrcft;
end



tmp = get(handles.bth_popup,'String');
bth_index = get(handles.bth_popup,'Value');
bathfile = char(tmp(bth_index));
% bathfile = char(tmp(usr.bth_index));

ssp= get(handles.ssp_popup,'String');
ssp_index = get(handles.ssp_popup,'Value');
ssp = char(ssp(ssp_index));
% ssp = char(ssp(usr.ssp_index));

paramfile='params';
eval(paramfile);

sspfile=sprintf('ssps/%s',ssp);
bathymfile=sprintf('ssps/%s',bathfile);

indx = find(ssp=='.');
sspsav = ssp(1:indx-1);
if usr.measType == 2
    ft = meters_to_feet(zrcvrft);
    ft = round(ft);
    outputfile =sprintf('%s_%s_rays_rcvr%ift.mat','singlepair',...
    sspsav,ft);
else
    rnd = round(zrcvrft);
    outputfile=sprintf('%s_%s_rays_rcvr%ift.mat','singlepair',...
    sspsav,rnd);
end
inputfile='in.ray';


%%% Initialize result matrix
  
  eval(paramfile);
  erayparams.zs=zsrcm;
  erayparams.zr=zrcvrm;
  erayparams.range=rsrcm;
  erayparams.sspfile=sspfile;
  erayparams.bathymfile=bathymfile;
 
  
  notdone=true;
  
 while(notdone)

     
     write_eigenray_input(erayparams,'in.ray',3);
     unix('eigenrayv3 < in.ray');

  tmp=load('ray.info');

  if isempty(tmp)
    disp('No answer:  increasing maxbounce by 5');
    pause(1);
    erayparams.maxbounce=erayparams.maxbounce+5;
  else
    notdone=false;
    matray
    raydata.Nray=Nray;
    raydata.tt=tt;
    raydata.r=r*1000;		% save in meters not km
    raydata.z=-z*1000;	    % save in meters not km, positive downward
    raydata.utd=-utd*1000;	% save in meters not km, positive downward
    raydata.ltd=-ltd*1000;	% save in meters not km, positive downward
    raydata.sa=sa;
    raydata.ra=ra;
   
    
    % Have to reverse range so reciever is at zero range.
    
    
   
    if (usr.measType == 2 ) %%% If Units are Meters
        plot(handles.spAxes,(flipud(r)*1000),-z*1000);
        xlabel('Range (m)')
        ylabel('Depth (m)');
        title(...
        sprintf('Range = %0.0f m ',...
        rsrcm));
        hold on
        x = plot(0,zrcvrm,'ro',rsrcm,zsrcm,'b*');
        legend(handles.spAxes,x ,'Receiver','Source','Location','Best');
        hold off
        
    end
    
    if (usr.measType == 1) %%% If Units are Feet
        
        
        %% Convert r z to feet
        
        r = r * 1000;
        z = z * 1000;
        
        r = meters_to_feet(r);
        z = meters_to_feet(z);
        
        plot(handles.spAxes,flipud(r),-z)
        
        xlabel('Range (ft)')
        ylabel('Depth (ft)');
        title(...
        sprintf('Range = %0.0f ft',...
        meters_to_feet(rsrcm)));
        hold on
        x = plot(0,meters_to_feet(zrcvrm),'ro',meters_to_feet(rsrcm),...
            meters_to_feet(zsrcm),'b*');
        legend(handles.spAxes,x ,'Receiver','Source','Location','Best');
        hold off
    end
    
    rplot

    axis('auto');

  end
 end
%%% Save the data
[filename path] = uiputfile(outputfile,'Single Ray save');

if ischar(path)
    tmpx = sprintf('%s%s',path,filename);
    save(tmpx,'raydata');
else
    warndlg('Simulation data was not saved.','Warning Dialog');
    return
end

% save(outputfile);
