%% Author: Joshua Reed, Date: 6/17/09
% 
%   The purpose of this program is to get gui values and write them in a
%   preset manner to a specified file.
% 
% 
%%
load paramSettings.mat



FILENAME = 'params.m';
fid = fopen(FILENAME, 'w');

temp{1} = str2num(get(handles.ang1, 'String'));
temp{2} = str2num(get(handles.ang2, 'String'));
temp{3} = str2num(get(handles.ray1, 'String'));
temp{4} = str2num(get(handles.dxsav, 'String'));
temp{5} = str2num(get(handles.zmiss, 'String'));
temp{6} = str2num(get(handles.zmissmax, 'String'));
temp{7} = str2num(get(handles.iraysave, 'String'));
temp{8} = str2num(get(handles.itimefront, 'String'));
temp{9} = str2num(get(handles.ibot, 'String'));
temp{10} = str2num(get(handles.ttoffset, 'String'));
temp{11} = str2num(get(handles.itemplate, 'String'));
temp{12} = str2num(get(handles.transdepth, 'String'));
temp{13} = str2num(get(handles.btol, 'String'));
temp{14} = str2num(get(handles.maxbounce, 'String'));
temp{15} = str2num(get(handles.ss_surf, 'String'));
temp{16} = str2num(get(handles.ss_trans, 'String'));
temp{17} = str2num(get(handles.ss_atdepth,'String'));
temp{18} = str2num(get(handles.epsilon, 'String'));
temp{19} = get(handles.template,'String');

% Write to programfile
fprintf(fid,'erayparams.ang1=%d;\n',temp{1});
fprintf(fid,'erayparams.ang2=%d;\n',temp{2});
fprintf(fid,'erayparams.numrays=%d;\n',temp{3});
fprintf(fid,'erayparams.dxsav=%d;\n',temp{4});
fprintf(fid,'erayparams.zmiss=%d;\n',temp{5});
fprintf(fid,'erayparams.zmissmax=%d;\n',temp{6});
fprintf(fid,'erayparams.iraysave=%d;\n',temp{7});
fprintf(fid,'erayparams.itimefront=%d;\n',temp{8});
fprintf(fid,'erayparams.ibot=%d;\n',temp{9});
fprintf(fid,'erayparams.ttoffset=%d;\n',temp{10});
fprintf(fid,'erayparams.itemplate=%d;\n',temp{11});
fprintf(fid,'erayparams.transdepth=%d;\n',temp{12});
fprintf(fid,'erayparams.btol=%d;\n',temp{13});
fprintf(fid,'erayparams.maxbounce=%d;\n',temp{14});
fprintf(fid,'erayparams.stepsize_surf=%d;\n',temp{15});
fprintf(fid,'erayparams.stepsize_trans=%d;\n',temp{16});
fprintf(fid,'erayparams.stepsize_atdepth=%d;\n',temp{17});
fprintf(fid,'erayparams.epsilon=%d;\n',temp{18});
fprintf(fid,'erayparams.templatefile=''%s'';\n',temp{19});
fprintf(fid,'erayparams.zs='''';\n');
fprintf(fid,'erayparams.zr='''';\n');
fprintf(fid,'erayparams.range='''';\n');

%Save to user defined file.

[filename,path]=uiputfile('Untitled.mat','Save Parameters');
    if ischar(path) 
    foo = sprintf('%s%s',path,filename);
    
    set(handles.userFile,'String',filename);
    
    erayparams.ang1=temp{1};
    erayparams.ang2=temp{2};
    erayparams.numrays=temp{3};
    erayparams.dxsav=temp{4};
    erayparams.zmiss=temp{5};
    erayparams.zmissmax=temp{6};
    erayparams.iraysave=temp{7};
    erayparams.itimefront=temp{8};
    erayparams.ibot=temp{9};
    erayparams.ttoffset=temp{10};
    erayparams.itemplate=temp{11};
    erayparams.transdepth=temp{12};
    erayparams.btol=temp{13};
    erayparams.maxbounce=temp{14};
    erayparams.stepsize_surf=temp{15};
    erayparams.stepsize_trans=temp{16};
    erayparams.stepsize_atdepth=temp{17};
    erayparams.epsilon=temp{18};
    erayparams.templatefile=temp{19};
    erayparams.zr='';
    erayparams.zs='';
    erayparams.range='';

    save(foo,'erayparams');
    
    else
        warndlg('Eigenray Params saved, but no User File was saved.',...
            'Warning Message');
    end



