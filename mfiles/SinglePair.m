function varargout = SinglePair(varargin)
% SINGLEPAIR M-file for SinglePair.fig
%      SINGLEPAIR, by itself, creates a new SINGLEPAIR or raises the existing
%      singleton*.
%
%      H = SINGLEPAIR returns the handle to a new SINGLEPAIR or the handle to
%      the existing singleton*.
%
%      SINGLEPAIR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SINGLEPAIR.M with the given input arguments.
%
%      SINGLEPAIR('Property','Value',...) creates a new SINGLEPAIR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SinglePair_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SinglePair_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SinglePair

% Last Modified by GUIDE v2.5 18-Aug-2009 15:08:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SinglePair_OpeningFcn, ...
                   'gui_OutputFcn',  @SinglePair_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SinglePair is made visible.
function SinglePair_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SinglePair (see VARARGIN)

% Choose default command line output for SinglePair
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SinglePair wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%% INITIALIZE HERE
load priorRuns
% Initialize program settings
usr.ssp_index = sPair.ssp;
usr.bth_index = sPair.bth;
usr.measType = 1;
usr.path = pwd;
save('progSettings','usr','handles');

% Initialize SSP popup menu
getSSPFileInfo
set(handles.ssp_popup,'String',ssp);

%Initialize Bathy popup menu
getBTHFileInfo
set(handles.bth_popup, 'String', bth);

% Load fields from prior run
set(handles.sprange,'String',num2str(sPair.range));
set(handles.spsrcdepth,'String',num2str(sPair.zsrc));
set(handles.sprcvrdepth,'String',num2str(sPair.zrcvr));
set(handles.ssp_popup,'Value',sPair.ssp);
set(handles.bth_popup,'Value',sPair.bth);
set(handles.units,'Value',sPair.em);
if (sPair.em == 0)
    load progSettings
    set(handles.units,'String','English');
    set(handles.units_r,'String','ft');
    set(handles.units_zsrc,'String','ft');
    set(handles.units_zrcvr,'String','ft');
    usr.measType = 1;
    save('progSettings','usr','handles');
else
    load progSettings
    set(handles.units,'String','Metric');
    set(handles.units_r,'String','m');
    set(handles.units_zsrc,'String','m');
    set(handles.units_zrcvr,'String','m');
    usr.measType = 2;
    save('progSettings','usr','handles');
end

% Format plot area
xlabel(handles.spAxes,'Range(m)');
ylabel(handles.spAxes,'Depth(m)');
title(handles.spAxes,'Range = ? m');
%%


% --- Outputs from this function are returned to the command line.
function varargout = SinglePair_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function units_Callback(hObject, eventdata, handles)
button_state = get(hObject, 'Value');

if button_state == get(hObject, 'Max')
        load progSettings.mat;
        usr.measType = 2; % Indicate Metric
        save('progSettings','usr','handles');
        tmp = str2num(get(handles.sprange, 'String')); 
        set(handles.sprange, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.spsrcdepth, 'String')); 
        set(handles.spsrcdepth, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.sprcvrdepth, 'String')); 
        set(handles.sprcvrdepth, 'String', feet_to_meters(tmp));
        set(handles.units,'String','Metric');
        set(handles.units_r,'String','m');
        set(handles.units_zsrc,'String','m');
        set(handles.units_zrcvr,'String','m');
elseif button_state == get(hObject,'Min')
        load progSettings.mat;
        usr.measType = 1; % Indicate English
        save('progSettings','usr','handles');
        tmp = str2num(get(handles.sprange, 'String')); 
        set(handles.sprange, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.spsrcdepth, 'String')); 
        set(handles.spsrcdepth, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.sprcvrdepth, 'String')); 
        set(handles.sprcvrdepth, 'String', meters_to_feet(tmp));
        set(handles.units,'String','English');
        set(handles.units_r,'String','ft');
        set(handles.units_zsrc,'String','ft');
        set(handles.units_zrcvr,'String','ft');
end


function ssp_popup_Callback(hObject, eventdata, handles)
function ssp_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function sprange_Callback(hObject, eventdata, handles)
function sprange_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function spsrcdepth_Callback(hObject, eventdata, handles)
function spsrcdepth_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function sprcvrdepth_Callback(hObject, eventdata, handles)
function sprcvrdepth_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function runSim_Callback(hObject, eventdata, handles)
cla(handles.spAxes,'reset');
% Format plot area
xlabel(handles.spAxes,'Range(m)');
ylabel(handles.spAxes,'Depth(m)');
title(handles.spAxes,'Range = ? m');
set(handles.spAxes,'XTickLabel',{'0','200','400','600','800','1000'});
set(handles.spAxes,'ydir','reverse');
set(handles.spAxes,'YTickLabel',{'0','20','40','60','80','100'});

% Save run
load priorRuns
sPair.ssp = get(handles.ssp_popup,'Value');
sPair.bth = get(handles.bth_popup,'Value');
sPair.em  = get(handles.units,'Value');
sPair.range=str2num(get(handles.sprange,'String'));
sPair.zsrc =str2num(get(handles.spsrcdepth,'String'));
sPair.zrcvr=str2num(get(handles.sprcvrdepth,'String'));
if isunix
    save('mfiles/priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
else
    save('mfiles\priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
end

tmp(1)= str2num(get(handles.sprange,'String')); 
tmp(2)= str2num(get(handles.spsrcdepth,'String')); 
tmp(3)= str2num(get(handles.sprcvrdepth,'String'));

for k = 1:length(tmp)
    if tmp(k) == 0
        errordlg('One or more or your values is Zero.  Use non-zero values.');
        return;
    end
end

singlepairRay

function bth_popup_Callback(hObject, eventdata, handles)
function bth_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function erayparams_Callback(hObject, eventdata, handles)
load progSettings
save('progSettings','usr','handles');
edparam

%% DROP DOWN MENU
function file_Callback(hObject, eventdata, handles)

function reset_Callback(hObject, eventdata, handles)
load priorRuns
% file single pair
sPair.ssp = 1;
sPair.bth = 1;
sPair.range=0;
sPair.zsrc=0;
sPair.zrcvr=0;
sPair.em=0;
% Reset Fields on GUI
set(handles.ssp_popup,'Value',1);
set(handles.bth_popup,'Value',1);
set(handles.units,'Value',0);
set(handles.units,'String','English');
set(handles.sprange,'String','0');
set(handles.spsrcdepth,'String','0');
set(handles.sprcvrdepth,'String','0');
set(handles.units_r,'String','ft');
set(handles.units_zsrc,'String','ft');
set(handles.units_zrcvr,'String','ft');
load progSettings
usr.measType = 1;
save('progSettings','usr','handles');
% Save file
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end

function exit_Callback(hObject, eventdata, handles)
close(findobj('type','figure','name','SinglePair'));

