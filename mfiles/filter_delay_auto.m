% filter_delay_auto(fc,fs,k_orig,l_offset,A,var,theta,c,delay)
function [y gensig chanmod lags]=filter_delay_auto(fc,fs,k_orig,l_offset,A,var,theta,c,delay)
    gensig =gen_srcsig(fc,fs,k_orig,l_offset);
    chanmod =chan_model(gensig,fs,A,delay,var,theta,c,k_orig,l_offset);
    [y lags]=autocorrelation(chanmod,delay);
end