function varargout = RDsim(varargin)
% RDSIM M-file for RDsim.fig
%      RDSIM, by itself, creates a new RDSIM or raises the existing
%      singleton*.
%
%      H = RDSIM returns the handle to a new RDSIM or the handle to
%      the existing singleton*.
%
%      RDSIM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RDSIM.M with the given input arguments.
%
%      RDSIM('Property','Value',...) creates a new RDSIM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RDsim_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RDsim_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RDsim

% Last Modified by GUIDE v2.5 18-Aug-2009 15:59:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RDsim_OpeningFcn, ...
                   'gui_OutputFcn',  @RDsim_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RDsim is made visible.
function RDsim_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RDsim (see VARARGIN)

% Choose default command line output for RDsim
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RDsim wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%% INITIALIZE HERE
load priorRuns
usr.ssp_index = rdepth.ssp;
usr.bth_index = rdepth.bth;
usr.measType = 1;
usr.break = 0;
usr.path = pwd;
save('progSettings','usr','handles');

% Initialize SSP popup menu
getSSPFileInfo
set(handles.ssp_popup,'String',ssp);
% Initialize Bth popup menu
getBTHFileInfo
set(handles.bth_popup,'String',bth);

% Load fields here
set(handles.srMax,'String',num2str(rdepth.rsrcMax));
set(handles.srMin,'String',num2str(rdepth.rsrcMin));
set(handles.srStep,'String',num2str(rdepth.rsrcStep));
set(handles.sdMax,'String',num2str(rdepth.zsrcMax));
set(handles.sdMin,'String',num2str(rdepth.zsrcMin));
set(handles.sdStep,'String',num2str(rdepth.zsrcStep));
set(handles.rcvrDepth,'String',num2str(rdepth.zrcvr));
set(handles.ssp_popup,'Value',rdepth.ssp);
set(handles.bth_popup,'Value',rdepth.bth);
set(handles.units,'Value',rdepth.em);
if (rdepth.em == 0)
    set(handles.units,'String','English');
    set(handles.units_rmin,'String','ft');
    set(handles.units_rmax,'String','ft');
    set(handles.units_dr,'String','ft');
    set(handles.units_zmax,'String','ft');
    set(handles.units_zmin,'String','ft');
    set(handles.units_dz,'String','ft');
    set(handles.units_zrcvr,'String','ft');
    load progSettings
    usr.measType = 1;
    save('progSettings','usr','handles');
else
    set(handles.units,'String','Metric');
    set(handles.units_rmin,'String','m');
    set(handles.units_rmax,'String','m');
    set(handles.units_dr,'String','m');
    set(handles.units_zmax,'String','m');
    set(handles.units_zmin,'String','m');
    set(handles.units_dz,'String','m');
    set(handles.units_zrcvr,'String','m');
    load progSettings
    usr.measType = 2;
    save('progSettings','usr','handles');
end

%%

% --- Outputs from this function are returned to the command line.
function varargout = RDsim_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function srMax_Callback(hObject, eventdata, handles)
function srMax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function srStep_Callback(hObject, eventdata, handles)
function srStep_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function srMin_Callback(hObject, eventdata, handles)
function srMin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function sdMax_Callback(hObject, eventdata, handles)
function sdMax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function sdStep_Callback(hObject, eventdata, handles)
function sdStep_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function sdMin_Callback(hObject, eventdata, handles)
function sdMin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function units_Callback(hObject, eventdata, handles)
button_state = get(hObject, 'Value');

if button_state == get(hObject, 'Max')
        load progSettings.mat;
        usr.measType = 2; % Indicate Metric
        save('progSettings','usr','handles');
        tmp = str2num(get(handles.srMax, 'String')); 
        set(handles.srMax, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.srMin, 'String')); 
        set(handles.srMin, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.srStep, 'String')); 
        set(handles.srStep, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.sdMax, 'String')); 
        set(handles.sdMax, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.sdMin, 'String')); 
        set(handles.sdMin, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.sdStep, 'String')); 
        set(handles.sdStep, 'String', feet_to_meters(tmp));
        tmp = str2num(get(handles.rcvrDepth, 'String')); 
        set(handles.rcvrDepth, 'String', feet_to_meters(tmp));
        set(handles.units,'String','Metric');
        set(handles.units_rmin,'String','m');
        set(handles.units_rmax,'String','m');
        set(handles.units_dr,'String','m');
        set(handles.units_zmax,'String','m');
        set(handles.units_zmin,'String','m');
        set(handles.units_dz,'String','m');
        set(handles.units_zrcvr,'String','m');
elseif button_state == get(hObject,'Min')
        load progSettings.mat;
        usr.measType = 1; % Indicate English
        save('progSettings','usr','handles');
        tmp = str2num(get(handles.srMax, 'String')); 
        set(handles.srMax, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.srMin, 'String')); 
        set(handles.srMin, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.srStep, 'String')); 
        set(handles.srStep, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.sdMax, 'String')); 
        set(handles.sdMax, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.sdMin, 'String')); 
        set(handles.sdMin, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.sdStep, 'String')); 
        set(handles.sdStep, 'String', meters_to_feet(tmp));
        tmp = str2num(get(handles.rcvrDepth, 'String')); 
        set(handles.rcvrDepth, 'String', meters_to_feet(tmp));
        set(handles.units,'String','English');
        set(handles.units_rmin,'String','ft');
        set(handles.units_rmax,'String','ft');
        set(handles.units_dr,'String','ft');
        set(handles.units_zmax,'String','ft');
        set(handles.units_zmin,'String','ft');
        set(handles.units_dz,'String','ft');
        set(handles.units_zrcvr,'String','ft');
end


function ssp_popup_Callback(hObject, eventdata, handles)
function ssp_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ssp_enter_Callback(hObject, eventdata, handles)
load progSettings.mat;
index = get(handles.ssp_popup,'Value');
usr.ssp_index = index;
save('progSettings','usr','handles');

function runSim_Callback(hObject, eventdata, handles)
tmp(1)= str2num(get(handles.srMax,'String')); 
tmp(2)= str2num(get(handles.srMin,'String')); 
tmp(3)= str2num(get(handles.srStep,'String'));
tmp(4)= str2num(get(handles.sdMax,'String')); 
tmp(5)= str2num(get(handles.sdMin,'String')); 
tmp(6)= str2num(get(handles.sdStep,'String'));
tmp(7)= str2num(get(handles.rcvrDepth,'String'));

for k = 1:length(tmp)
    if tmp(k) == 0
        errordlg('One or more or your values is Zero.  Use non-zero values.');
        return;
    end
end
%%%       Range      ||      Depth
%%%  Min       Max       Min       Max
if ( tmp(2) > tmp(1) || tmp(5) > tmp(4))
    errordlg('Minimum value is larger than Maximum.');
    return
end

% Save run
load priorRuns
rdepth.ssp = get(handles.ssp_popup,'Value');
rdepth.bth = get(handles.bth_popup,'Value');
rdepth.em  = get(handles.units,'Value');
rdepth.zsrcMax = str2num(get(handles.sdMax,'String'));
rdepth.zsrcMin = str2num(get(handles.sdMin,'String'));
rdepth.zsrcStep= str2num(get(handles.sdStep,'String'));
rdepth.rsrcMax = str2num(get(handles.srMax,'String'));
rdepth.rsrcMin = str2num(get(handles.srMin,'String'));
rdepth.rsrcStep= str2num(get(handles.srStep,'String'));
rdepth.zrcvr   = str2num(get(handles.rcvrDepth,'String'));
if isunix
    save('mfiles/priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
else
    save('mfiles\priorRuns.mat','LMirror','sPair','srcTrack','rdepth');
end

raysim_rangedepth

function rcvrDepth_Callback(hObject, eventdata, handles)
function rcvrDepth_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cancel_Callback(hObject, eventdata, handles)
load progSettings.mat;
usr.break = 1;
save('progSettings','usr','handles');



function bth_popup_Callback(hObject, eventdata, handles)
function bth_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function bth_enter_Callback(hObject, eventdata, handles)
load progSettings.mat;
index = get(handles.bth_popup,'Value');
usr.bth_index = index;
save('progSettings','usr','handles');

%% DROP DOWN MENU
function file_Callback(hObject, eventdata, handles)

function reset_Callback(hObject, eventdata, handles)
load priorRuns
% Reset RD grid
rdepth.ssp =1;
rdepth.bth = 1;
rdepth.em = 0;
rdepth.rsrcMax = 0;
rdepth.rsrcMin = 0;
rdepth.rsrcStep=0;
rdepth.zsrcMax = 0;
rdepth.zsrcMin =0;
rdepth.zsrcStep=0;
rdepth.zrcvr =0;
% Reset fields
set(handles.ssp_popup,'Value',1);
set(handles.bth_popup,'Value',1);
set(handles.units,'Value',0);
set(handles.units,'String','English');
set(handles.srMax,'String','0');
set(handles.srMin,'String','0');
set(handles.srStep,'String','0');
set(handles.sdMax,'String','0');
set(handles.sdMin,'String','0');
set(handles.sdStep,'String','0');
set(handles.rcvrDepth,'String','0');
set(handles.units_rmin,'String','ft');
set(handles.units_rmax,'String','ft');
set(handles.units_dr,'String','ft');
set(handles.units_zmax,'String','ft');
set(handles.units_zmin,'String','ft');
set(handles.units_dz,'String','ft');
set(handles.units_zrcvr,'String','ft');
load progSettings
usr.measType = 1;
save('progSettings','usr','handles');
% Save
if isunix
    save('mfiles/priorRuns','sPair','LMirror','srcTrack','rdepth');
else
    save('mfiles\priorRuns','sPair','LMirror','srcTrack','rdepth');
end

function exit_Callback(hObject, eventdata, handles)
close(findobj('type','figure','name','RDsim'));




