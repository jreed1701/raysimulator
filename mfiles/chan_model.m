 %Script file:chan_model.m
 %
 %Purpose: Determine the output of delay system
 % Record of revisions:
 % Date         Programmer        Description of change
 %=====         ===========        ====================
 %07/24/2009    Thuykhanh Le      Original code
  function y2=chan_model(y1,fs,A,delay,var,theta,c,k_orig,l_offset)
    %scale: scale of direct and reflected rays of delay system
    %delay: delay time of reflected signal
    %k_orig : the original length of random signal
    %l_offset : the length of transient response
    load progSettings
    
%%Making the echo function 
N=length(y1)+delay(length(delay));
n=0:N-1;
Temp=zeros(1,N);
Y1=fft(y1,N);
omega=(0:2*pi/N:(N-1).*2*pi./N);
ind=find(omega>pi);
omega(ind)=omega(ind)-2*pi;
Td=1/fs;
B_omega=-(exp(-2*((omega./Td).^2).*(var)*((cos(theta)).^2)./c^2));
for i=1:length(delay)
       if i==1     
           Y(i,:)=A*Y1;
       else
           Y(i,:)=B_omega.*Y1.*exp(-j*omega*delay(i));
       end
      Y_temp=Y(i,:);
     Temp=Temp+Y_temp;
     Y=Temp;
     y2=ifft(Y,N);
end
   stem(handles.rcvdSig,n,y2)
   title(handles.rcvdSig,'Simulated Recieved Signal')
   ylabel(handles.rcvdSig,'y[n]')
   axis(handles.rcvdSig,'tight');
%%Checking the maximum imaginary part of the output;
% if max(imag(y2))>=10^(-14)
%    warndlg('The imaginary part of the output is too high','Warning');
%    y_maximag=max(imag(y2));
%    fprintf('The maximum imaginary part of the output is %g\n',y_maximag)
%    y_maxreal=max(real(y2));
%    fprintf('The maximum real part of the output is %g\n',y_maxreal)
% end
y2=y2((l_offset+1):k_orig+l_offset);%Cut-off the first l_offset points
  end
