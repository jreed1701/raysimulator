%% Author: Joshua Reed, Date: 8/6/09
% 
%   [ theta ] = getSurfaceAngle( surfaceray ) 
% 
%   Inputs: surfaceray = structure containing surface path rays
% 
%   Outputs: theta = Theta angle made between surface path ray and ocean
%   surface
%
%   fuction:  This program will calculate an average theta angle between
%   given surface ray paths and the ocean surface.

function [ theta ] = getSurfaceAngle( surfaceray )

    leg = length(surfaceray);
    
    for k = 1:leg
        % Make sure there is data first.
        if (~isempty(surfaceray(k,1).depth))
            cur_depth = surfaceray(k,1).depth;
            cur_range = surfaceray(k,1).range;
            min_val = min(cur_depth);
            index = find(min_val == cur_depth);
            % Check to see if vector has two indicies with same min value.
            if(length(index) > 1) 
                index = index(1); % Only need index one index
            end
            leg_short = cur_depth(index-1); 
            min_leg_long = cur_range(index);
            max_leg_long = cur_range(index-1);
            leg_long = abs(max_leg_long - min_leg_long);
            
            temp_ang(k) = atan(leg_short/leg_long);
        end
    end
    % Want to average values, but don't average zeros.  Turn zero's to NaN
    for k = 1:length(temp_ang);
        if(temp_ang(k) == 0)
            temp_ang(k) = NaN;
        end
    end
    theta = nanmean(temp_ang);   
end