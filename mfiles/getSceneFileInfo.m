%% Author: Joshua Reed, Date: 6/22/09
% 
%   This program will get scenariofiles. 
% 
% 
directory = 'scenarios/*.mat';

sFiles = dir(directory);
temp  = struct2cell(sFiles);

for k = 1:length(sFiles),...
        scene{k,1} = temp{1,k};
end