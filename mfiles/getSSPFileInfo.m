%% Author: Joshua Reed , Date: 6/16/09
% 
%   This file will obtain all SSP files and store them in string format.
% 

directory = 'ssps/*.ssp';

sspFiles = dir(directory);
temp  = struct2cell(sspFiles);
for k = 1:length(sspFiles),...
        ssp{k,1} = temp{1,k};
end

% clear temp sspFiles directory;
