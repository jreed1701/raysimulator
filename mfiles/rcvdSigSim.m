%%% Script file to run ray simulation

clear;
load progSettings.mat;

%% Specify parameters
rsrcft =str2num(get(handles.range,'String'));         % Range 
zrcvrft=str2num(get(handles.rcvr_Depth,'String'));     % Reciever Depth
zsrcft =str2num(get(handles.src_Depth,'String'));    % Source Depth


if rsrcft < 0
    errordlg('Maximum range cannot be negative!','Input Error');
    return
end


if usr.measType == 1 % Have to convert feet to meters
zrcvrm=feet_to_meters(zrcvrft);
zsrcm=feet_to_meters(zsrcft);
rsrcm=feet_to_meters(rsrcft);
else
    zrcvrm=zrcvrft;
    zsrcm=zsrcft;
    rsrcm=rsrcft;
end
%% Specify Files


tmp = get(handles.bth_popup,'String');
bth_index = get(handles.bth_popup,'Value');
bathfile = char(tmp(bth_index));

ssp= get(handles.ssp_popup,'String');
ssp_index = get(handles.ssp_popup,'Value');
ssp = char(ssp(ssp_index));


paramfile='params';
eval(paramfile);

sspfile=sprintf('ssps/%s',ssp);
bathymfile=sprintf('ssps/%s',bathfile);

indx = find(ssp=='.');
sspsav = ssp(1:indx-1);
if usr.measType == 2
    ft = meters_to_feet(zrcvrft);
    ft = round(ft);
    outputfile =sprintf('%s_%s_rays_rcvr%ift.mat','LloydMirror',...
    sspsav,ft);
else
    rnd = round(zrcvrft);
    outputfile=sprintf('%s_%s_rays_rcvr%ift.mat','LloydMirror',...
    sspsav,rnd);
end
inputfile='in.ray';


%% Initialize result matrix / Run Eigenray / Plot paths
  
  eval(paramfile);
  erayparams.zs=zsrcm;
  erayparams.zr=zrcvrm;
  erayparams.range=rsrcm;
  erayparams.sspfile=sspfile;
  erayparams.bathymfile=bathymfile;
 
  
  notdone=true;
  
 while(notdone)
     
     write_eigenray_input(erayparams,'in.ray',3);
     unix('eigenrayv3 < in.ray');

  tmp=load('ray.info');

  if isempty(tmp)
    disp('No answer:  increasing maxbounce by 5');
    pause(1);
    erayparams.maxbounce=erayparams.maxbounce+5;
  else
    notdone=false;
    matray
    raydata.Nray=Nray;
    raydata.tt=tt;
    raydata.r=r*1000;		% save in meters not km
    raydata.z=-z*1000;	    % save in meters not km, positive downward
    raydata.utd=-utd*1000;	% save in meters not km, positive downward
    raydata.ltd=-ltd*1000;	% save in meters not km, positive downward
    raydata.sa=sa;          
    raydata.ra=ra;
   
    
    
    
    
    
    %%%Have to reverse range so reciever is at zero range.
    if (usr.measType == 2) %%% Units are meters
        plot(handles.pair, (flipud(r)*1000),-z*1000);
        xlabel(handles.pair,'Range (m)');
        hold(handles.pair,'on');
        x = plot(handles.pair, 0,zrcvrm,'ro',rsrcm,zsrcm,'b*');
        legend(handles.pair,x ,'Receiver','Source','Location','Best');
        hold(handles.pair,'off');
    end
    
    if (usr.measType == 1) %%% Units are Feet
        
        r1 = r * 1000;
        z1 = z * 1000;
        
        r1 = meters_to_feet(r1);
        z1 = meters_to_feet(z1);
        
        plot(handles.pair,flipud(r1),-z1);
        xlabel(handles.pair,'Range (ft)');
        hold(handles.pair,'on');
        x = plot(handles.pair, 0,meters_to_feet(zrcvrm),'ro',...
        meters_to_feet(rsrcm),meters_to_feet(zsrcm),'b*');
        legend(handles.pair,x ,'Receiver','Source','Location','Best');
        hold(handles.pair,'off');
    end
    
    
    set(handles.pair,'ydir','reverse');
    axis(handles.pair,'tight');
    
    plotSSP  %%% NEED TO PLOT SSP BESIDE PATH PLOT
    
  end
 end
%% Get path delay


mintt=min(raydata.tt);
utd0=find(raydata.utd==0);



if length(utd0)>1
    ttdiff0=diff(raydata.tt(utd0));
    if max(ttdiff0)>=.0002
        str=sprintf('ttdiff >= 0.2 milliseconds');
        disp(str)
    %	 disp(raydata(zind,rind).tt(utd0))
    end
end

if ~isempty(utd0)
    %%% Note:  if there is more than one surface path, this code chooses the
    %%% one with the minimum deltat.  The old code chose the first one, and
    %%% that led to problems.
    deltat=min(raydata.tt(utd0)-mintt);
    %deltat(zind,rind)=raydata(zind,rind).tt(utd0(1))-mintt;
else
    deltat=NaN;
    errordlg('No time delay calculated between rays.  Variable deltat = NaN !');
    return
end
%% Run Le's Code -- Calculates Recieved Signal / Plots Rcvd Sig & AutoCorr.

%%% Fixed inputs for now
l_offset = 30;
A =1;
c = getSurfSpeed(sspfile);


%%% User inputs
fs = str2num(get(handles.fs,'String'));
fc = str2num(get(handles.fc,'String'));
k_orig = str2num(get(handles.numPoints,'String'));
rms = str2num(get(handles.rms,'String'));

var = rms^2;

xtmpx = deltat * fs; %% sampled version

delay = [ 0 xtmpx];  %% Format delay; 

foo = sprintf('The time delay is %0.1f (samples)',xtmpx);
title(handles.pair,foo);

%%% Run code to calculate theta %%%
[all surface refract bottom multiple ] = classifyRays( raydata , bathfile );
if ~isempty(surface)
    [ theta ] = getSurfaceAngle( surface );
    [y gensig chanmod lags] = filter_delay_auto(fc,fs,k_orig,l_offset,A,var,theta,c,delay);
    
    [filename path] = uiputfile(outputfile,'Lloyd Mirror save');

    if ischar(path)
        tmpx = sprintf('%s%s',path,filename);
        save(tmpx,'raydata','y','gensig','chanmod','lags','all');
    else
        warndlg('Simulation data was not saved.','Warning Dialog');
        return
    end

else
    errordlg('No single Lloyd Mirror path found. Only ray paths & SSP will be plotted.',...
    'Data Not Saved');
    return
end

