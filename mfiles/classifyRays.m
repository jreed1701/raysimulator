%% Author: Joshua Reed, Date: 8/5/09
% 
%   [all surface refract bottom  multiple] = classifyRays( raydata , bathyfile ) 
% 
%   Inputs: raydata = raydata saved from EIGENRAY program
%           bathyfile = bathymetry file used to get raydata
%   
%   Outputs: all = structure containing all rays
%            surface = structure containing surface reflected rays
%            refract = structure containing refracted rays
%            bottom  = structure containing bottom bounce rays
%            multiple= structure containing rays with multiple bounces,
%                      both surface and bottom.
% 
%   fuction:  This program will classify rays as either a surface
%   reflection path, refraction path, or bottom bounce path.

function [all surface refract bottom multiple] = classifyRays( raydata , bathyfile )
    
    eval('params');
    max_depth = getOceanBottom(bathyfile);
    
    %%% TOLERANCES! 
    topmiss = 1; % Surface miss tolerance
    botmiss = max_depth - erayparams.btol; % Bottom miss tolerance

    % Seperate data into structures.
    [M N ] = size(raydata);

    raydata.r = flipud(raydata.r); % Reverse range so rcvr is at 0 range.
    for r = 1:M
        for c = 1:N
            ranges = raydata(r,c).r;
            depths = raydata(r,c).z;
            for k = 1:raydata(r,c).Nray
                rays(k,r).range = ranges(:,k);
                rays(k,r).depth = depths(:,k);
                minray = min(rays(k,r).depth);
                maxray = max(rays(k,r).depth);
                % Check for rays going above ocean surface or below ocean
                % bottom and ignore them.
                if((minray < -3)||(maxray > (max_depth + 3)))
                    % The ignored vector will not be included with results.
                    ignore(k,r).range = rays(k,r).range;
                    ignore(k,r).depth = rays(k,r).depth;
                % check for surface reflection
                elseif( (minray < topmiss) && (maxray > botmiss))
                    multiple(k,r).range = rays(k,r).range;
                    multiple(k,r).depth = rays(k,r).range;
                elseif (minray < topmiss) 
                    surface(k,r).range = rays(k,r).range;
                    surface(k,r).depth = rays(k,r).depth;
                % If ray path is bottom bounce
                elseif (maxray > botmiss)
                    bottom(k,r).range = rays(k,r).range;
                    bottom(k,r).depth = rays(k,r).depth;                    
                else
                    refract(k,r).range = rays(k,r).range;
                    refract(k,r).depth = rays(k,r).depth;
                end       
%                 figure(k)
%                 set(gcf,'WindowStyle','Docked');
%                 plot(rays(k,r).range,rays(k,r).depth);
%                 set(gca,'ydir','reverse');  
            end     
        end
    end
    %% Give variables an empty setting if they don't exist after loops
    if (exist('bottom','var') == 0)
        bottom = [];
    end
    if (exist('surface','var') == 0)
        surface = [];
    end
    if (exist('refract','var') == 0)
        refract = [];
    end
    if (exist('multiple','var') == 0)
        multiple = [];
    end
    all = rays;
end
