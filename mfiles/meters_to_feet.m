function feet=meters_to_feet(meters)

feet=meters*100/(2.54*12);

return;

