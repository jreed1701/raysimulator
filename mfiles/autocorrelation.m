 %Script file:autocorrelation.m
 %
 %Purpose: Determine the autocorrelation of the output of delay system
 % Record of revisions:
 % Date         Programmer        Description of change
 %=====         ===========        ====================
 %07/24/2009    Thuykhanh Le      Original code
function [y lags]=autocorrelation(y2,delay)
    %y2 is the output of delay system
    
    load progSettings    

    y=xcorr(y2);%Determine the autocorrelation of output of delay system
    lags=[-(length(y)-1)/2:(length(y)-1)/2];

    plot(handles.auto,lags,abs(y))
    % 
    % figure(1)
    % plot(abs(y));


    foo = max(abs(y));


    axis(handles.auto,[ -500 500 0 foo ])


    xx = vline(round(delay(2)),'r--','');
    yy = vline(-round(delay(2)),'r--','');

    legend([xx yy],'True Delay','Location','NorthEast');

    title(handles.auto,'Output of Autocorrelation Processor')
    ylabel(handles.auto,'R_{yy}[n]')
    xlabel(handles.auto,'n')
    
end

