%% Author: Joshua Reed, Date: 8/6/09
% 
%   [ max_depth ] = getOceanBottom( bathyfile ) 
% 
%   Inputs: bathyfile = name of bathymetry file
%   
%   Outputs: max_depth = Depth of ocean bottom or deepest point in bathy
%   file
%
%   fuction:  This program will simply find the deepest point in a
%   bathymetry file.  Usually the ocean bottom.



function [ max_depth ] = getOceanBottom( bathyfile )


bathyfiledir = sprintf('ssps/%s',bathyfile);

index = find('.' == bathyfile);
name = bathyfile(1:index-1);

load(bathyfiledir)

var = eval(name);


max_depth = max(var);

end

