% A program to read in all the variables of the "ray.info" file
% of the eigenray prediction.  
%
% (Place this program in ~/matlab for general access to it.)
%
% The user also is given the option of loading in the ray path data of the "ray.data" file.
% The ray data appear in arrays r(:,1:N) and z(:,1:N) for the N rays.
% The ray data arrays are filled out by NaNs - i.e. after the ray ends in the array 
% you get NaNs.  The length of the array is the number of points for the ray having
% the most points.
%
% After reading in the "ray.info" file the following arrays are defined:
%  inx  =  ray indexes
%  tt   =  ray travel times (seconds)
%  sa   =  angles of rays at source (positive is ray towards surface)
%  ra   =  angles of rays at receiver (positive is ray towards surface)
%  ltd  =  lower turning depths of rays (negative, in km)
%  utd  =  upper turning depths of rays (negative, in km)
%  zr   =  depths of rays at receiver  (negative, in km)
%  Nray =  total number of rays found
%
% If the ray paths are also loaded in you get arrays:
% 
%  nray(1:Nray) = the lengths of the Nray ray paths
%  r(:,1:Nray)  = the range in km for the Nray ray paths
%  z(:,1:Nray)  = the depth in km (negative down) for the Nray ray paths

load ray.info
inx=ray(:,1);
tt=ray(:,2);
sa=ray(:,3);
ra=ray(:,4);
ltd=-ray(:,5)/1000;
utd=-ray(:,6)/1000;
zr=-ray(:,7)/1000;
ntp=ray(:,9);
Nray=length(tt);
disp('  inx  =  ray index')
disp('  tt   =  ray travel time (seconds)')
disp('  sa   =  angle of ray at source (positive is ray towards surface)')
disp('  ra   =  angle of ray at receiver (positive is ray towards surface)')
disp('  ltd  =  lower turning depth of ray (negative, in km)')
disp('  utd  =  upper turning depth of ray (negative, in km)')
disp('  zr   =  depth of ray at receiver  (negative, in km)')
disp('  Nray =  total number of rays')
disp(' ')
disp(['Found ' num2str(Nray) ' rays'])

disp(' ')

%test=input('Load Ray Paths? y/Y or n/N ','s');
test='y';

if test=='y' | test=='Y',
load ray.data
I=ray(:,1)==99999;
IP=find(I);
iend=-1;
mxln=0;
for i=1:length(IP)
  istart=iend+2;
  iend=IP(i)-1;
  mxln=max([mxln iend-istart+1]);
end
r=nan*ones(mxln,length(IP));
z=nan*ones(mxln,length(IP));
w=nan*ones(mxln,length(IP));
iend=-1;
nray=[];
for i=1:length(IP)
  istart=iend+2;
  iend=IP(i)-1;
  lray=iend-istart+1;
  nray=[nray lray];
  r(1:lray,i)=ray(istart:iend,1);
  z(1:lray,i)=ray(istart:iend,2);
  w(1:lray,i)=ray(istart:iend,3);
end
% convert variables from meters to km
r=r/1000;
z=z/1000;

disp(' ')
disp(' nray(1:Nray)  is the number of points in each ray path')
disp(' r(1:nray(n),n) is range of nth ray along raypath (in km)')
disp(' z(1:nray(n),n) is depth of nth ray along raypath (in km; negative down)')
disp(' w(1:nray(n),n) is the ray weighting (ds/c^2) along path' )
disp(' These matrices are filled in with NaNs')
disp('plot(r,z) will plot all rays; the NaNs get omitted')
disp('plot(r(:,n),z(:,n)) will plot the nth ray')
disp(' ')

end

%test=input('Load a Bathymetry file? y/Y or n/N ','s');
test='n';

if test=='y' | test=='Y',
name=input('Input the Bathymetry filename: ','s');
B=load(name);
rbath=B(:,1)/1000;
zbath=-B(:,2)/1000;

disp(' ')
disp(' rbath(:) is the range of bathymetry (in km)')
disp(' zbath(:) is the depth of bathymetry (in km; negative down)')
disp('plot(rbath,zbath) will plot the bathymetry')
disp(' ')
end

