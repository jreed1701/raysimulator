%% Author: Joshua Reed, Date: 7/27/09

% This program will plot ssp files
load progSettings

%%% Get SSP file.
ssp = get(handles.ssp_popup,'String');
ssp_index = get(handles.ssp_popup,'Value');
sspfilename = char(ssp(ssp_index));



index = find(sspfilename == '.');
name = sspfilename(1:index-1);



sspfiledir = sprintf('ssps/%s',sspfilename);
load(sspfiledir);


var = eval(name);

space = 15;

depth  = var(:,1);
sspeed = var(:,2);

% Find out how many SSP's are in file.
index = find(-1 == depth);

if(length(index) == 1)                          %%% 1 SSP IN FILE

    depth = depth(2:length(depth));
    sspeed = sspeed(2:length(sspeed));

    if (usr.measType == 1) %%% Units are Feet    
        sspeed = sspeed * 3.281 ;  %%% Convert m/s to ft/s;
        depth = meters_to_feet(depth);
        zrcvr = meters_to_feet(zrcvrm);
        zsrc = meters_to_feet(zsrcm);
        plot(handles.ssp, sspeed,depth);
        xlabel(handles.ssp,'SSP(ft/s)');
        ylabel(handles.ssp,'Depth (ft)');
        z1 = z * - 1000;
        z1 = meters_to_feet(z1);
        ymax = max(z1);
        ymax = max(ymax);
    else

        plot(handles.ssp, sspeed,depth);
        xlabel(handles.ssp,'SSP(m/s)');
        ylabel(handles.ssp,'Depth (m)');
        ymax = max(z*-1000);
        ymax = max(ymax);
    end
    
    
    

    set(handles.ssp,'ydir','reverse');


    xmin = min(sspeed) - space;
    xmax = max(sspeed) + space;
    ymin = 0;
    
    axis(handles.ssp,[ xmin xmax ymin ymax]);
    
end

if( length(index) == 2)                         %%% 2 SSP'S IN FILE
        sspeed1 = sspeed(1:index(2)-1);
        sspeed2 = sspeed(index(2):length(sspeed));
        depth1 = depth(1:index(2)-1);
        depth2 = depth(index(2):length(depth));

        depth1 = depth1(2:length(depth1));
        depth2 = depth2(2:length(depth2));
        sspeed1 = sspeed1(2:length(sspeed1));
        sspeed2 = sspeed2(2:length(sspeed2));
        

        if (usr.measType == 1) %%% Units are Feet    
                sspeed1 = sspeed1 * 3.281 ;  %%% Convert m/s to ft/s;
                sspeed2 = sspeed2 * 3.281 ;
                depth1 = meters_to_feet(depth1);
                depth2 = meters_to_feet(depth2);
                plot(handles.ssp, sspeed1,depth1,'b:');
                hold on
                plot(handles.ssp, sspeed2,depth2,'r--');
                hold off
                xlabel(handles.ssp,'SSP(ft/s)');
                ylabel(handles.ssp,'Depth (ft)');
                z1 = z * - 1000;
                z1 = meters_to_feet(z1);
                ymax = max(z1);
                ymax = max(ymax);
        else
            plot(handles.ssp, sspeed1,depth1,'b:');
            hold on
            plot(handles.ssp, sspeed2,depth2,'r--');
            hold off
            xlabel(handles.ssp,'SSP(m/s)');
            ylabel(handles.ssp,'Depth (m)');
            ymax = max(z*-1000);
            ymax = max(ymax);
        end
        
        set(handles.ssp,'ydir','reverse');
        
        if (max(sspeed1) > max(sspeed2))
            xmax = max(sspeed1) + space;
        else
            xmax = max(sspeed2) + space;
        end
        
        if (min(sspeed1) < min(sspeed2))
            xmin = min(sspeed1) - space;
        else
            xmin = min(sspeed2) - space;
        end
        
        ymin = 0;
        axis(handles.ssp,[ xmin xmax ymin ymax]);
        
%         if (usr.measType == 1)
%             f1 = round(xmin);
%             f2 = round(xmax);
%             keyboard
%             set(handles.ssp,'XTick',f1);
%         end
        
end


title(handles.ssp,sspfilename);



