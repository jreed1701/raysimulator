%%% Script file to run ray simulation

clear;
load progSettings.mat
%%% Specify Directories
scenario_dir='scenarios';
rayoutput_dir= usr.path;

%%% Specify parameters

ssp= get(handles.ssp_popup, 'String');
ssp_index = get(handles.ssp_popup,'Value');
ssp = char(ssp(ssp_index));

bathy= get(handles.bth_popup, 'String');
bth_index = get(handles.bth_popup,'Value');
bathy = char(bathy(bth_index));

scenario= get(handles.track_popup, 'String');
scn_index = get(handles.track_popup,'Value');
scenario_tmp = char(scenario(scn_index));
indx = find(scenario_tmp=='.');
scenario = scenario_tmp(1:indx-1);

paramfile='params';


sspfile=sprintf('ssps/%s',ssp);
scenariofile=sprintf('%s/%s',scenario_dir,scenario);
bathymfile = sprintf('ssps/%s',bathy);

% Format output file
indx = find(ssp=='.');
sspsav = ssp(1:indx-1);
outputfile=sprintf('%s_%s_%s_rays.mat','track',scenario,sspsav);



inputfile='in.ray'; %Input to eigenray


%%% Load scenario information
load(scenariofile);
ntim=length(tim);


%%% For loop to run ray simulations
count=0;
for ind=1:ntim
%for ind=21:21
  count=count+1;
  
  eval(paramfile);
  erayparams.zs=zsrcm(ind);
  erayparams.zr=zrcvrm;
  erayparams.range=rsrcm(ind);
  erayparams.sspfile=sspfile;
  erayparams.bathymfile=bathymfile;
  
  
  notdone=true;
  
 while(notdone)
     
    load progSettings.mat;
    if usr.break == 1
        usr.break = 0;
        save('progSettings','usr','handles');
        cla(handles.stAxes,'reset');        % Reformat Plot Area
        xlabel(handles.stAxes,'Range(m)');
        ylabel(handles.stAxes,'Depth(m)');
        title(handles.stAxes,'Range = ? m');
        axis([0 1000 0 100]);
        set(gca,'YTickLabel',{'100','80','60','40','20','0'});
        warndlg('The simulation was cancelled.','Warning');
        set(handles.cancel,'String','Sim Cancelled');
        pause(2);
        set(handles.cancel,'String','Cancel Simulation');
        return
    end
     
     write_eigenray_input(erayparams,'in.ray',3);
     unix('eigenrayv3 < in.ray');

  tmp=load('ray.info');

  if isempty(tmp)
    disp('No answer:  increasing maxbounce by 5');
    pause(1);
    erayparams.maxbounce=erayparams.maxbounce+5;
  else
    notdone=false;
    matray
    raydata(count).Nray=Nray;
    raydata(count).tt=tt;
    raydata(count).r=r*1000;		% save in meters not km
    raydata(count).z=-z*1000;	    % save in meters not km, positive downward
    raydata(count).utd=-utd*1000;	% save in meters not km, positive downward
    raydata(count).ltd=-ltd*1000;	% save in meters not km, positive downward
    raydata(count).sa=sa;
    raydata(count).ra=ra;
    
    plot(handles.stAxes,r*1000,-z*1000);
    xlabel('Range (m)')
    ylabel('Depth (m)');
    title(...
	sprintf('Range = %0.0f m (%0.0f ft) Index=%0.0f',...
	rsrcm(ind),meters_to_feet(rsrcm(ind)),ind));
    rplot
    hold on
    x = plot(0,zsrcm(ind),'b*');
    y = plot(rsrcm(ind),zrcvrm,'ro');
    hold off
    legend(handles.stAxes,[x y],'Source','Reciever');
    axis(handles.stAxes,'tight');
  end
end
end

%%% Save the data
[filename path] = uiputfile(outputfile,'Source Track save');
if ischar(path)
    tmpx = sprintf('%s%s',path,filename);
    save(tmpx,'raydata');
else
    warndlg('Simulation data was not saved.','Warning Dialog');
    return
end

