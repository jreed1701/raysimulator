%% Author: Joshua Reed , Date: 6/16/09
% 
%   This file will obtain all .Bth files and store them in string format.
% 

directory = 'ssps/*.bth';

bthFiles = dir(directory);
temp  = struct2cell(bthFiles);

for k = 1:length(bthFiles),...
        bth{k,1} = temp{1,k};
end

% clear temp bthFiles directory;