function varargout = edparam(varargin)
%EDPARAM M-file for edparam.fig
%      EDPARAM, by itself, creates a new EDPARAM or raises the existing
%      singleton*.
%
%      H = EDPARAM returns the handle to a new EDPARAM or the handle to
%      the existing singleton*.
%
%      EDPARAM('Property','Value',...) creates a new EDPARAM using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to edparam_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      EDPARAM('CALLBACK') and EDPARAM('CALLBACK',hObject,...) call the
%      local function named CALLBACK in EDPARAM.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help edparam

% Last Modified by GUIDE v2.5 31-Jul-2009 13:46:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @edparam_OpeningFcn, ...
                   'gui_OutputFcn',  @edparam_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function edparam_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for edparam
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes edparam wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%% INITIALIZE HERE
              
params

set(handles.ang1, 'String', erayparams.ang1);
set(handles.ang2, 'String', erayparams.ang2);
set(handles.ray1, 'String', erayparams.numrays);
set(handles.dxsav, 'String', erayparams.dxsav);
set(handles.zmiss, 'String', erayparams.zmiss);
set(handles.zmissmax, 'String', erayparams.zmissmax);
set(handles.iraysave, 'String', erayparams.iraysave);
set(handles.itimefront, 'String', erayparams.itimefront);
set(handles.ibot, 'String', erayparams.ibot);
set(handles.ttoffset, 'String', erayparams.ttoffset);
set(handles.itemplate, 'String', erayparams.itemplate);
set(handles.transdepth, 'String', erayparams.transdepth);
set(handles.btol, 'String', erayparams.btol);
set(handles.maxbounce, 'String', erayparams.maxbounce);
set(handles.ss_surf, 'String', erayparams.stepsize_surf);
set(handles.ss_trans, 'String', erayparams.stepsize_trans);
set(handles.ss_atdepth, 'String', erayparams.stepsize_atdepth);
set(handles.epsilon, 'String', erayparams.epsilon);

save('paramSettings.mat','handles');


%%
function varargout = edparam_OutputFcn(hObject, eventdata, handles)

% Get default command line output from handles structure
varargout{1} = handles.output;


function Write_Callback(hObject, eventdata, handles)
ang1 = str2num(get(handles.ang1,'String'));
ang2 = str2num(get(handles.ang2,'String'));

if ( ang1 > 0 || ang2 < 0)
    errordlg('Make sure Angle 1 is between -1 a -89 and Angle 2 is between 1 and 89');
    pause(1)
    params
    if ( ang1 > 0 )
        set(handles.ang1,'String',erayparams.ang1);
    end
    if ( ang2 < 0 )
        set(handles.ang2,'String',erayparams.ang2);
    end
    return
end
    
if (ang1 < -89 || ang2 > 89)
    errordlg('Launch angles must be between +- 89 degrees');
    pause(1)
    params  %% Put old values back
    if (ang1 < -89 )
    set(handles.ang1,'String',erayparams.ang1);
    end
    if (ang2 > 89)
    set(handles.ang2,'String',erayparams.ang2);
    end
    return
end



set(handles.Write, 'String', 'Saving...');
writeToFile
set(handles.Write, 'String', 'Params Saved');
pause(1);
set(handles.Write, 'String', 'Write To File');

function Load_Callback(hObject, eventdata, handles)

question_ans = questdlg('This action will reset all values. Proceed?',...
    'User check','Yes','No','');

if strcmp(question_ans, 'Yes')
    % To let user know that Defualts are being reloaded.
    set(handles.Load, 'String', 'Loading...');
    resetParams
    saveparams
    pause(1);
    set(handles.Load, 'String', 'Load Prior Run');
end

function epsilon_Callback(hObject, eventdata, handles)
function epsilon_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ray1_Callback(hObject, eventdata, handles)
function ray1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ray2_Callback(hObject, eventdata, handles)
function ray2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ang1_Callback(hObject, eventdata, handles)
function ang1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ang2_Callback(hObject, eventdata, handles)
function ang2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function zs_Callback(hObject, eventdata, handles)
function zs_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function zr_Callback(hObject, eventdata, handles)
function zr_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function range_Callback(hObject, eventdata, handles)
function range_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function dxsav_Callback(hObject, eventdata, handles)
function dxsav_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function zmiss_Callback(hObject, eventdata, handles)
function zmiss_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function zmissmax_Callback(hObject, eventdata, handles)
function zmissmax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function iraysave_Callback(hObject, eventdata, handles)
function iraysave_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function itimefront_Callback(hObject, eventdata, handles)
function itimefront_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ibot_Callback(hObject, eventdata, handles)
function ibot_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ttoffset_Callback(hObject, eventdata, handles)
function ttoffset_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function itemplate_Callback(hObject, eventdata, handles)
function itemplate_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function transdepth_Callback(hObject, eventdata, handles)
function transdepth_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ss_surf_Callback(hObject, eventdata, handles)
function ss_surf_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ss_trans_Callback(hObject, eventdata, handles)
function ss_trans_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ss_atdepth_Callback(hObject, eventdata, handles)
function ss_atdepth_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function btol_Callback(hObject, eventdata, handles)
function btol_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function maxbounce_Callback(hObject, eventdata, handles)
function maxbounce_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function sspfile_Callback(hObject, eventdata, handles)
function sspfile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function bthfile_Callback(hObject, eventdata, handles)
function bthfile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function template_Callback(hObject, eventdata, handles)
function template_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function outFile_Callback(hObject, eventdata, handles)
function outFile_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function loadSel_Callback(hObject, eventdata, handles)


function close_Callback(hObject, eventdata, handles)
  close('edparam');  


