%%% Script file to run ray simulation

% THIS FILE DOESN'T USE .BTH FILES, SO NEED TO MAKE SURE ERAY IS NOT LOOKING
% FOR IT!!


clear;
load progSettings.mat;
%%% Specify parameters
rminft= str2num(get(handles.srMin,'String')); % Range Min
rmaxft= str2num(get(handles.srMax,'String')); % Range Max
zminft= str2num(get(handles.sdMin,'String')); % Depth Min
zmaxft= str2num(get(handles.sdMax,'String')); % Depth Max
dzft  = str2num(get(handles.sdStep  ,'String')); % Depth Step size
drft  = str2num(get(handles.srStep  ,'String')); % Range Step size
zrcvrft=str2num(get(handles.rcvrDepth,'String'));% Reciever Depth

%% REPORT NUMBER OF SIMULATIONS
%%% Looping through to see how many iterations will be required to finish
%%% the simulation.
numsim = 0;
for zsrcft=zminft:dzft:zmaxft
    for rsrcft=rmaxft:-drft:rminft
        numsim = numsim + 1; 
    end
end
set(handles.simcount,'String',num2str(numsim));
%%


if usr.measType == 1 %If value was in feet
zrcvrm=feet_to_meters(zrcvrft);
else
    zrcvrm = zrcvrft; 
end

% if user.measType == 2 % If inputs were all in meters
%     zrcvrm = 
% end

%% Get Profiles & Parameters
ssp= get(handles.ssp_popup,'String');
ssp_index = get(handles.ssp_popup,'Value');
ssp = char(ssp(ssp_index));

bth = get(handles.bth_popup,'String');
bth_index = get(handles.bth_popup,'Value');
bth = char(bth(bth_index));

paramfile='params';
eval(paramfile);

sspfile=sprintf('ssps/%s',ssp); 
bathyfile = sprintf('ssps/%s',bth);

% Get default output file.
indx = find(ssp=='.');
sspsav = ssp(1:indx-1);
if usr.measType == 2  
    ft = meters_to_feet(zrcvrft);
    ft = round(ft);
    outputfile=sprintf('%s_%s_rays_rcvr%dft.mat','rdgrid',...
    sspsav,ft);
else
    rnd = round(zrcvrft);
    outputfile=sprintf('%s_%s_rays_rcvr%dft.mat','rdgrid',...
    sspsav,rnd);
end
inputfile='in.ray';
%% Initialize result matrix
%%% For loop to run ray simulations
simsdone = 1;
countz=0;
for zsrcft=zminft:dzft:zmaxft
  countz=countz+1;
  countr=0;
  for rsrcft=rmaxft:-drft:rminft
    countr=countr+1;


    if usr.measType == 1
        erayparams.zs=feet_to_meters(zsrcft);
        erayparams.range=feet_to_meters(rsrcft);
    else
            erayparams.zs=zsrcft;
            erayparams.range=rsrcft;
    end

    erayparams.zr=zrcvrm;
    erayparams.sspfile=sspfile;
    
    load progSettings.mat;
    if usr.break == 1
        usr.break = 0;
        save('progSettings','usr','handles');
        set(handles.simcount,'String','');
        set(handles.toGo,'String','');
        warndlg('The simulation was cancelled.','Warning');
        set(handles.cancel,'String','Sim Cancelled');
        pause(2);
        set(handles.cancel,'String','Cancel Simulation');
        return
    end

    erayparams.bathymfile = bathyfile;

    write_eigenray_input(erayparams,'in.ray',3);
    unix('eigenrayv3 < in.ray')

    matray
    raydata(countz,countr).Nray=Nray;
    raydata(countz,countr).tt=tt;
    raydata(countz,countr).r=r*1000;		% save in meters not km
    raydata(countz,countr).z=-z*1000;       % save in meters not km, pos. down
    raydata(countz,countr).utd=-utd*1000;   % save in meters not km, pos. down
    raydata(countz,countr).ltd=-ltd*1000;   % save in meters not km, pos. down
    raydata(countz,countr).sa=sa;
    raydata(countz,countr).ra=ra;
    raydata(countz,countr).zsrcm=erayparams.zs;
    raydata(countz,countr).rsrcm=erayparams.zr;

%     plot(r*1000,-z*1000);  %%% In case I want to plot someday.
%     xlabel('Range (m)')
%     ylabel('Depth (m)');
%     title(...
% 	sprintf('Range = %0.0f ft, Depth = %0.0f ft',...
% 	rsrcft,zsrcft));
%     rplot
%     axis([0 4000 0 200])
%     pause
      
      %%% Tell user how many simulations are remaining.
      report = numsim - simsdone;
      set(handles.toGo,'String',num2str(report));
      simsdone = simsdone + 1;
  end
end
% Save results

set(handles.simcount,'String','');
set(handles.toGo,'String','');

set(handles.simcount,'String','');
[filename path] = uiputfile(outputfile,'Range Depth Save');
if ischar(path)
    tmpx = sprintf('%s%s',path,filename);
    isrdgrid = true;
    save(tmpx,'raydata','countz','countr','sspsav','rmaxft','rminft','drft',...
        'zmaxft','zminft','zmaxft','dzft','isrdgrid');
else
    warndlg('Simulation data was not saved.','Warning Dialog');
    return
end
