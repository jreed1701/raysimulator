 %Script file:gen_srcsig.m
 %
 %Purpose:Generate the source signal by using random signal and lowpass
 %filter
 % Record of revisions:
 % Date         Programmer        Description of change
 %=====         ===========        ====================
 %07/24/2009    Thuykhanh Le      Original code
  function y1=gen_srcsig(fc,fs,k_orig,l_offset)
    %fs: sampling rate frequency
    %fc: the cutoff frequency of lowpass filter
    %k_orig : the original length of random signal
    %l_offset : the length of transient response
 %%%Determine the cofficient of the IIR lowpass filter
 Td=1/fs;   %sampling rate period
  b=[Td*2*pi*fc       0]; %coefficient of numerator of lowpass filter
  a=[1            -exp(-2*pi*fc*Td)];%coef.of denomenator of lowpass filter
%%%Determine the output of the IIR lowpass filter with the input is white
%%%noise signal
      x=randn(1,k_orig+l_offset);%create the random signal with the length is (k_orig+l_offset)
      y1=filter(b,a,x);%using filter to get output of random signal
      y1=y1(l_offset+1:length(y1));
  end
