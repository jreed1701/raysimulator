function [deltat,ranges,depths]=rzgrid_to_deltat(rayfile)

load(rayfile);


if exist('isrdgrid','var')

        outputfile = sprintf('%s_%s_rays.mat','timedelay',...
            sspsav);

        %%% The following code loops through the ray data to get delta t between
        %%% the direct arrival and the surface reflection (only surface
        %%% reflections considered)
        for zind=1:countz
          for rind=1:countr
            mintt=min(raydata(zind,rind).tt);

            utd0=find(raydata(zind,rind).utd==0);
            if length(utd0)>1
              ttdiff0=diff(raydata(zind,rind).tt(utd0));
              if max(ttdiff0)>=.0002
                 str=sprintf('ttdiff >= 0.2 milliseconds for countr %i, countz %i',...
                 rind,zind);
                 disp(str)
        %	 disp(raydata(zind,rind).tt(utd0))
               end
            end

            if ~isempty(utd0)
        %%% Note:  if there is more than one surface path, this code chooses the
        %%% one with the minimum deltat.  The old code chose the first one, and
        %%% that led to problems.
              deltat(zind,rind)=min(raydata(zind,rind).tt(utd0)-mintt);
        %      deltat(zind,rind)=raydata(zind,rind).tt(utd0(1))-mintt;
            else
              deltat(zind,rind)=NaN;
            end
          end
        end

        ranges=rmaxft:-drft:rminft;
        depths=zminft:dzft:zmaxft;
        disp('Converting deltat to milliseconds')
        deltat=deltat*1000;   

        %%% Save data
        [filename path] = uiputfile(outputfile,'Save Range Depth Simulation');
        if ischar(path)
            tmpx = sprintf('%s%s',path,filename);
            save(tmpx,'deltat','ranges','depths');
        else
            warndlg('Simulation data was not saved.','Warning Dialog');
                %%% Still want to show plot.
                figure(1)
                subplot(211)
                imagesc(ranges,depths,deltat);
                title('Image Plot (top), Line Plot (bottom). Delays in miliseconds');
                xlabel('Range (m)');
                ylabel('Depth (m)');
                colorbar('vert');
                subplot(212)
                x = plot(ranges,deltat);
                ylabel('Delta t (ms)');
                xlabel('Range (m)');
                legend(x,dd);
            return
        end

else
    errordlg('The input file does not contain the required variables. Try again.',...
    'Incorrect inputs');
    return
end

figure(1)
subplot(211)
imagesc(ranges,depths,deltat);
title('Image Plot (top), Line Plot (bottom). Delays in miliseconds');
xlabel('Range (m)');
ylabel('Depth (m)');
colorbar('vert');
subplot(212)
plot(ranges,deltat);
ylabel('Delta t (ms)');
xlabel('Range (m)');




return
